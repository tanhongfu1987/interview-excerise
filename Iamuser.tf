/* Create IAM User */ 
resource "aws_iam_user" "merquriuser" {
  name = "merquriuser"
}

/* Specify what policy you are going to use */ 
resource "aws_iam_policy" "ReadOnlyAccess" {
  name = "ReadOnlyAccess"
}

/* Attach policy to IAM User */ 
resource "aws_iam_user_policy_attachment" "test-attach" {
  user       = aws_iam_user.user.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}
